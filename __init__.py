# -*- coding: utf-8 -*-
"""
    line
    ~~~~

    May the LINE be with you...

    :special thank to Taehoon Kim, Modify by ShinZ.
    :license: BSD.
"""
from .client import LineClient
from .api import LineAPI
from .models import LineGroup, LineContact, LineRoom, LineBase, LineMessage

VERSION = (0, 4, 1)

__copyright__ = 'Copyright 2014 source by Taehoon Kim, Modify by ShinZ'
__version__ = '.'.join(map(str, VERSION))
__license__ = 'BSD'
__author__ = 'Shinz'
__author_email__ = 'shinznatkid@gmail.com'
__url__ = ''

__all__ = [
    # LineClient object
    'LineClient',
    # model wrappers for LINE API
    'LineGroup', 'LineContact', 'LineRoom', 'LineBase', 'LineMessage',
    # Line Thrift API
    'LineAPI',
]

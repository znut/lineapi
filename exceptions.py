# -*- coding: utf-8 -*-


class LineException(Exception):
    pass


class LoginFailedException(LineException):
    pass


class LoginExpiredException(LoginFailedException):
    pass


class AnotherLoginException(LoginExpiredException):
    pass


class AddContactUserExistsException(LineException):
    pass


class ContactNotFoundException(LineException):
    pass


class FindContactBlockedException(LineException):
    pass


class ContactCannotDeleteException(LineException):
    pass

# -*- coding: utf-8 -*-
"""
    DEPRECATE !!!
    !!! DON'T NEED ANYMORE !!!
"""
"""
    line.client
    ~~~~~~~~~~~

    LineClient for sending and receiving message from LINE server.

    :copyright: (c) 2014 by Taehoon Kim.
    :license: BSD, see LICENSE for more details.
"""
import re
import rsa
import sys
import time

from api import LineAPI
from curve import CurveThrift
from models import LineGroup, LineContact, LineRoom, LineMessage
from curve.ttypes import TalkException, ToType, OperationType, Provider, Message
from .exceptions import LoginFailedException, LoginExpiredException, AnotherLoginException, AddContactUserExistsException, ContactNotFoundException, FindContactBlockedException

reload(sys)
sys.setdefaultencoding("utf-8")

EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")


def auth_required(f):
    def wrapper(self, *args):
        if self.authToken:
            try:
                return f(self, *args)
            except TalkException as e:
                # code=1, 'Authentication Failed.'
                if e.code == 8 or e.code == 1:
                    result = self.login()
                    if result['success']:
                        return f(self, *args)
                    else:
                        raise LoginExpiredException('Login Failed.')
                raise
        else:
            raise LoginExpiredException('Authenticated required.')
    return wrapper


class LineBaseClient(LineAPI):

    def __init__(self, email=None, password=None, com_name='WINDOWS-PC', auth_token=None, *args, **kwargs):

        super(LineBaseClient, self).__init__(*args, **kwargs)

        self.com_name  = com_name
        self.authToken = auth_token
        self.email     = email
        self.password  = password

        os_version = "5.1.2600-XP-x64"
        user_agent = "DESKTOP:WIN:%s(%s)" % (os_version, self.version)
        app = "DESKTOPWIN\t%s\tWINDOWS\t%s" % (self.version, os_version)

        self._headers['User-Agent']         = user_agent
        self._headers['X-Line-Application'] = app
        self._headers['X-Line-Access']      = self.authToken

        self.id = self.account.email
        EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")
        if EMAIL_REGEX.match(self.id):
            self.provider = Provider.LINE  # LINE
        else:
            self.provider = Provider.NAVER_KR  # NAVER
        self.setProtocol()

    @property
    def certificate(self):
        if not hasattr(self, '_certificate'):
            self._certificate = self.account.certificate
        return self._certificate

    @certificate.setter
    def certificate(self, value):
        if not hasattr(self, '_certificate'):
            self._certificate = self.account.certificate
        if self._certificate != value:
            self._certificate = value
            self.account.certificate = self._certificate
            self.account.save(update_fields=['certificate'])

    def _login(self, email, password):
        """Login to LINE server."""

        self.id       = email
        self.password = password

        if self.provider == CurveThrift.Provider.LINE:  # LINE
            j = self._get_json(self.LINE_SESSION_LINE_URL)
        else:  # NAVER
            j = self._get_json(self.LINE_SESSION_NAVER_URL)

        session_key = j['session_key']
        message     = (chr(len(session_key)) + session_key +
                       chr(len(self.id)) + self.id +
                       chr(len(self.password)) + self.password).encode('utf-8')

        keyname, n, e = j['rsa_key'].split(",")
        pub_key       = rsa.PublicKey(int(n, 16), int(e, 16))
        crypto        = rsa.encrypt(message, pub_key).encode('hex')

        username = keyname
        password = crypto

        try:
            msg = self._loginClient.loginWithIdentityCredentialForCertificate(
                username, password, keyname, crypto, True, self.ip,
                self.com_name, self.provider, self.certificate
            )
        except TalkException as e:
            if e.code == 18:  # Account ID and password does not match
                raise LoginFailedException('Account ID and password does not match')
            elif e.code == 4:  # Authentication fault exceeded
                raise LoginFailedException('Authentication fault exceeded')
            raise e

        if msg.type == 1:
            if msg.certificate is not None:
                with open(self.CERT_FILE, 'w') as f:
                    f.write(msg.certificate)
                self.certificate = msg.certificate
            self.authToken = self._headers['X-Line-Access'] = msg.authToken
            return {
                'success': True,
            }
        elif msg.type == 2:
            msg = "require QR code"
            # self.raise_error(msg)
            return {
                'success': False,
            }
        elif msg.type == 3:  # pinCode
            self._headers['X-Line-Access'] = msg.verifier
            self._pinCode = msg.pinCode
            print "Enter PinCode '%s' to your mobile phone in 2 minutes" % self._pinCode

            return {
                'success': False,
                'pincode': msg.pinCode,
            }
        else:
            msg = "require device confirm"
            # self.raise_error(msg)
            return {
                'success': False,
            }

    def _check_auth(self):
        if self.authToken:
            return True
        else:
            raise Exception("you need to login")

    @property
    def profile(self):
        if not hasattr(self, '_profile'):
            if self._check_auth():
                self._profile  = LineContact(self, self._getProfile())
        return self._profile

    def updateStatusMessage(self, msg):
        """Get `profile` of LINE account"""
        if self._check_auth():
            return self._updateStatusMessage(msg)

        return False

    def getContactByName(self, name):
        """Get a `contact` by name

        :param name: name of a contact
        """
        for contact in self.contacts:
            if name == contact.name:
                return contact

        return None

    def getContactById(self, id):
        """Get a `contact` by id

        :param id: id of a contact
        """
        for contact in self.contacts:
            if contact.id == id:
                return contact

        return None

    def getContactOrRoomOrGroupById(self, id, update_if_not_found=False):
        """Get a `contact` or `room` or `group` by its id
        :param id: id of a instance
        """
        result = self.getContactById(id) or self.getRoomById(id) or self.getGroupById(id)
        if not result and update_if_not_found:
            time.sleep(1)
            self.refreshGroups()
            time.sleep(1)
            self.refreshContacts()
            time.sleep(1)
            self.refreshActiveRooms()
            try:
                return self.getContactOrRoomOrGroupById(id)
            except:
                return None
        return result

    def getBlockedContacts(self):
        if self._check_auth():
            contact_ids = self._getBlockedContactIds()
            contacts    = self._getContacts(contact_ids)
            contacts    = [LineContact(self, contact) for contact in contacts]
            contacts.sort()
            return contacts

    @auth_required
    def refreshGroups(self):
        """Refresh groups of LineClient"""
        self.groups = []

        self.addGroupsWithIds(self._getGroupIdsJoined())
        # time.sleep(0.5)
        self.addGroupsWithIds(self._getGroupIdsInvited(), False)

    def addGroupsWithIds(self, group_ids, is_joined=True):
        """Refresh groups of LineClient"""
        if self._check_auth():
            new_groups  = self._getGroups(group_ids)

            for group in new_groups:
                self.groups.append(LineGroup(self, group, is_joined))

            self.groups.sort()

    def refreshContacts(self):
        """Refresh contacts of LineClient """
        def chunk(iterable, n):
            from itertools import islice
            iterable = iter(iterable)
            while True:
                yield list(islice(iterable, n)) or iterable.next()

        if self._check_auth():
            import datetime

            contacts = []
            start_time = datetime.datetime.now()
            contact_ids = self._getAllContactIds()
            # print 'DEBUG: get ids time %s' % (datetime.datetime.now() - start_time)
            # TODO: try dynamic update only
            if contact_ids:
                for contact_ids_partition in chunk(contact_ids, 1000):
                    contacts += self._getContacts(contact_ids_partition)
                    sys.stdout.write('.')
            # print 'DEBUG: get contacts time %s' % (datetime.datetime.now() - start_time)

            self.contacts = []

            for contact in contacts:
                self.contacts.append(LineContact(self, contact))

            self.contacts.sort()

    def findAndAddContactByUserid(self, userid):
        """Find and add a `contact` by userid
        :param userid: user id
        """
        if self._check_auth():
            try:
                contact = self._findAndAddContactsByUserid(userid)
            except TalkException as e:
                self.raise_error(e.reason)

            contact = contact.values()[0]

            for c in self.contacts:
                if c.id == contact.mid:
                    raise AddContactUserExistsException("%s already exists" % contact.displayName)

            c = LineContact(self, contact)
            self.contacts.append(c)

            self.contacts.sort()
            return c

    def _findAndAddContactByPhone(self, phone):
        """Find and add a `contact` by phone number
        :param phone: phone number (unknown format)
        """
        if self._check_auth():
            try:
                contact = self._findAndAddContactsByPhone(phone)
            except TalkException as e:
                self.raise_error(e.reason)

            contact = contact.values()[0]

            for c in self.contacts:
                if c.id == contact.mid:
                    self.raise_error("%s already exists" % contact.displayName)
                    return

            c = LineContact(self, contact.values()[0])
            self.contacts.append(c)

            self.contacts.sort()
            return c

    def _findAndAddContactByEmail(self, email):
        """Find and add a `contact` by email
        :param email: email
        """
        if self._check_auth():
            try:
                contact = self._findAndAddContactsByEmail(email)
            except TalkException as e:
                self.raise_error(e.reason)

            contact = contact.values()[0]

            for c in self.contacts:
                if c.id == contact.mid:
                    self.raise_error("%s already exists" % contact.displayName)
                    return

            c = LineContact(self, contact.values()[0])
            self.contacts.append(c)

            self.contacts.sort()
            return c

    def findAndAddContactsByMid(self, mid):
        """
        Find a `contact` by mid
        """
        if self._check_auth():
            try:
                contacts = self._findAndAddContactsByMid(mid)
            except TalkException as e:
                if e.reason.startswith('Cannot find:'):
                    raise ContactNotFoundException(e.reason)
                else:
                    self.raise_error(e.reason)
            result = None
            for key, contact in contacts.items():
                result = LineContact(self, contact)
            return result

    def findContactByUserid(self, userid):
        """Find a `contact` by userid
        :param userid: user id
        """
        if self._check_auth():
            try:
                contact = self._findContactByUserid(userid)
            except TalkException as e:
                if(
                    e.reason.startswith('Cannot find:') or
                    e.reason == 'Invalid userid'
                ):
                    raise ContactNotFoundException(e.reason)
                elif e.reason == 'Blocked':
                    raise FindContactBlockedException(e.reason)
                else:
                    self.raise_error(e.reason)

            return LineContact(self, contact)

    def findContactByPhone(self, phones):
        """Find a `contact` by phones
        :param phones: phones
        """
        if self._check_auth():
            try:
                contact = self._findContactsByPhone(phones)
            except TalkException as e:
                if e.reason.startswith('Cannot find:'):
                    raise ContactNotFoundException(e.reason)
                else:
                    self.raise_error(e.reason)

            return LineContact(self, contact)

    def refreshActiveRooms(self):
        """Refresh active chat rooms"""
        print 'Rooms now disabled cuz LINE: Internal Error.'
        return
        if self._check_auth():
            start = 1
            count = 50

            self.rooms = []

            while True:
                time.sleep(0.5)
                channel = self._getMessageBoxCompactWrapUpList(start, count)
                if channel.messageBoxWrapUpList:
                    for box in channel.messageBoxWrapUpList:
                        if box.messageBox.midType == ToType.ROOM:
                            time.sleep(0.5)
                            room = LineRoom(self, self._getRoom(box.messageBox.id))
                            self.rooms.append(room)

                if channel.messageBoxWrapUpList is not None and len(channel.messageBoxWrapUpList) == count:
                    start += count
                else:
                    break

    def createGroupWithIds(self, name, ids=[]):
        """Create a group with contact ids

        :param name: name of group
        :param ids: list of contact ids
        """
        if self._check_auth():
            try:
                group = LineGroup(self, self._createGroup(name, ids))
                self.groups.append(group)

                return group
            except Exception as e:
                self.raise_error(e)

                return None

    def createGroupWithContacts(self, name, contacts=[]):
        """Create a group with contacts

        :param name: name of group
        :param contacts: list of contacts
        """
        if self._check_auth():
            try:
                contact_ids = []
                for contact in contacts:
                    contact_ids.append(contact.id)

                group = LineGroup(self, self._createGroup(name, contact_ids))
                self.groups.append(group)

                return group
            except Exception as e:
                self.raise_error(e)

                return None

    def getGroupByName(self, name):
        """Get a group by name

        :param name: name of a group
        """
        for group in self.groups:
            if name == group.name:
                return group

        return None

    def getGroupById(self, id):
        """Get a group by id

        :param id: id of a group
        """
        for group in self.groups:
            if group.id == id:
                return group

        return None

    def inviteIntoGroup(self, group, contacts=[]):
        """Invite contacts into group

        :param group: LineGroup instance
        :param contacts: LineContact instances to invite
        """
        if self._check_auth():
            contact_ids = [contact.id for contact in contacts]
            self._inviteIntoGroup(group.id, contact_ids)

    def acceptGroupInvitation(self, group):
        """Accept a group invitation

        :param group: LineGroup instance
        """
        if self._check_auth():
            try:
                self._acceptGroupInvitation(group.id)
                return True
            except Exception as e:
                self.raise_error(e)
                return False

    def leaveGroup(self, group):
        """Leave a group

        :param group: LineGroup instance to leave
        """
        if self._check_auth():
            try:
                self._leaveGroup(group.id)
                self.groups.remove(group)

                return True
            except Exception as e:
                self.raise_error(e)

                return False

    def createRoomWithIds(self, ids=[]):
        """Create a chat room with contact ids"""
        if self._check_auth():
            try:
                room = LineRoom(self, self._createRoom(ids))
                self.rooms.append(room)

                return room
            except Exception as e:
                self.raise_error(e)

                return None

    def createRoomWithContacts(self, contacts=[]):
        """Create a chat room with contacts"""
        if self._check_auth():
            try:
                contact_ids = []
                for contact in contacts:
                    contact_ids.append(contact.id)

                room = LineRoom(self, self._createRoom(contact_ids))
                self.rooms.append(room)

                return room
            except Exception as e:
                self.raise_error(e)

                return None

    def getRoomById(self, id):
        """Get a room by id

        :param id: id of a room
        """
        for room in self.rooms:
            if room.id == id:
                return room

        return None

    def inviteIntoRoom(self, room, contacts=[]):
        """Invite contacts into room

        :param room: LineRoom instance
        :param contacts: LineContact instances to invite
        """
        if self._check_auth():
            contact_ids = [contact.id for contact in contacts]
            self._inviteIntoRoom(room.id, contact_ids)

    def leaveRoom(self, room):
        """Leave a room

        :param room: LineRoom instance to leave
        """
        if self._check_auth():
            try:
                self._leaveRoom(room.id)
                self.rooms.remove(room)

                return True
            except Exception as e:
                self.raise_error(e)

                return False

    def sendMessage(self, message, seq=0):
        if not message:  # don't send empty message
            return
        """Send a message

        :param message: LineMessage instance to send
        """
        if self._check_auth():
            return self._sendMessage(message, seq)

    def getMessageBox(self, id):
        """Get MessageBox by id

        :param id: `contact` id or `group` id or `room` id
        """
        if self._check_auth():
            try:
                messageBoxWrapUp = self._getMessageBoxCompactWrapUp(id)

                return messageBoxWrapUp.messageBox
            except:
                return None

    def getRecentMessages(self, messageBox, count):
        """Get recent message from MessageBox

        :param messageBox: MessageBox object
        """
        if self._check_auth():
            id = messageBox.id
            messages = self._getRecentMessages(id, count)

            return self.getLineMessageFromMessage(messages)